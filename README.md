# Messias Dias | Curriculum Vitae

Desenvolvido com VueJs.
<hr>

Ferramentas utilizadas:
* FontAwesome,
* Animate-scss
* Jquery
* Typed.js

[messiasdias.github.io/curriculum](https://messiasdias.github.io/curriculum)

![Screenshot](screenshots/screenshot.png?raw=true "Curriculum")


![Screenshot](screenshots/screenshot2.png?raw=true "Curriculum")


<hr>

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
