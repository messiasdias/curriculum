import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'

import { 
    faTimes, 
    faEnvelope,
    faPrint,
    faAddressBook,
    faNetworkWired,
    faCodeBranch,
    faInfo,
    faGraduationCap,
    faBriefcase,
    faDollarSign,
    faBullseye,
    faHeart,

} 
from '@fortawesome/free-solid-svg-icons'

import { fab } from '@fortawesome/free-brands-svg-icons'

library.add(
    fab,
    faTimes,
    faEnvelope,
    faPrint,
    faAddressBook,
    faNetworkWired,
    faCodeBranch,
    faInfo,
    faGraduationCap,
    faBriefcase,
    faDollarSign,
    faBullseye,
    faHeart
    
)


export default FontAwesomeIcon
